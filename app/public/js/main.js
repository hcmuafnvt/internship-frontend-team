(function($) {	
	$('.app-wrapper').css({'height' : $(window).height()});	

	var request = $.ajax({
		method: "GET",
		url: "api/users",		
		cache: false
	});

	request.done(function(userList){
		var userListLength = userList.length;
		var userTable = $("#usertable tbody");
		var userInfo = "";
		for (var i = 0; i < userListLength; i++) {
			var user = userList[i];
			userInfo += "<tr><td>" + user.fname + "</td><td>" + user.lname + "</td><td>" + user.gender + "</td></tr>"
		}
		userTable.append(userInfo);
	});
	
	request.fail(function(jqXHR, textStatus, errorThrown){
		alert("Something wrong!\n" + jqXHR + "\n" + textStatus + "\n" + errorThrown);
	});
}(jQuery));