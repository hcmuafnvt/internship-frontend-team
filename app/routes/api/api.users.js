var express = require('express');
var router = express.Router();

var usersModel = require('../../models/model.users.js');

/* GET users. */
router.get('/', function(req, res, next) {	
  	res.json(usersModel.getUsers());
});

/* GET user. */
router.get('/:username', function(req, res, next) {
	var user = usersModel.getUser(req.params.username);
	if(user != null) {
		res.json({status : 'success', user : user});
	}
  	else
  	{
  		res.json({status : 'fail', message : 'User is not found'});
  	}
});

/* POST user. */
router.post('/', function(req, res, next) {	
	var username = req.body.username;
	if(typeof username !== 'undefined')
	{
		var isExisted = usersModel.checkUserName(req.body.username);
		if(isExisted) {
			res.json({status : 'false', message : 'username is existed'});
		}
		else
		{
			usersModel.addUser(req.body);
	  		res.json({status : 'success'});
		}		
	}
	
});

/* DELETE user. */
router.delete('/:username', function(req, res, next) {
	var isDeleted = usersModel.deleteUser(req.params.username);
	if(isDeleted) {
		res.json({status : 'success'});
	}
  	else
  	{
  		res.json({status : 'fail'});
  	}
});

/* PUT user. */
router.put('/:username', function(req, res, next) {	
	var isUpdated = usersModel.updateUser(req.params.username, req.body);
	if(isUpdated) {
		res.json({status : 'success'});
	}
  	else
  	{
  		res.json({status : 'fail'});
  	}
});

module.exports = router;