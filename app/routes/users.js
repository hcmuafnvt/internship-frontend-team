var express = require('express');
var router = express.Router();

var usersModel = require('../models/model.users.js');

router.get('/add', function(req, res, next) {
  	res.render('frmAddUser.html');
});

module.exports = router;
