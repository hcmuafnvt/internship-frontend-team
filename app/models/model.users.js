module.exports = (function() {
	var instance = {}, users = [
		{username : 'an', fname : 'An', lname : 'Nguyen', email : 'an@abc.com', password : '123', conformPassword : '123', country : 'Viet Nam', gender : 'Man' },
		{username : 'hieu', fname : 'Hieu', lname : 'Nguyen', email : 'hieu@abc.com', password : '123', conformPassword : '123', country : 'Viet Nam', gender : 'Man' },
		{username : 'sang', fname : 'Sang', lname : 'Nguyen', email : 'sang@abc.com', password : '123', conformPassword : '123', country : 'Viet Nam', gender : 'Man' }
	];

	//private function
	function getIndexOfUser(username) {
		var i;
		for(i = 0; i < users.length; i++) {
			if(users[i].username === username) {
				return i;
			}
		}

		return -1;
	}

	instance.getUsers = function() {
		return users;
	};

	instance.getUser = function(username) {
		var index = getIndexOfUser(username);
		if(index !== -1) {
			return users[index];
		}
		return null;
	};

	instance.addUser = function(user) {	
		users.push(user);
	};

	instance.deleteUser = function(username) {
		var index = getIndexOfUser(username);
		if(index !== -1) {
			users.splice(index, 1);
			return true;
		}		

		return false;
	}

	instance.updateUser = function(username, user) {
		var index = getIndexOfUser(username);
		if(index !== -1) {
			users[index] = user;
			return true;
		}		
		return false;
	}

	instance.checkUserName = function(username) {
		var index = getIndexOfUser(username);
		if(index !== -1)
		{
			return true;
		}

		return false;
	}

	return instance;
}());